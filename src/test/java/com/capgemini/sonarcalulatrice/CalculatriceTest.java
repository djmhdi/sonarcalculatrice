package com.capgemini.sonarcalulatrice;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatriceTest {

	
	Calculatrice calc = new Calculatrice();
	
//	@Test
//	public void testAddition() {
//		assertEquals(8, calc.addition(2, 6));
//	}

	@Test
	public void testSoustraction() {
		assertEquals(8, calc.soustraction(10, 2));
	}

	@Test
	public void testMultiplication() {
		assertEquals(8, calc.multiplication(2, 4));
	}

	@Test
	public void testDivision() {
		assertEquals(8, calc.division(16, 2));
	}
	
	

}
