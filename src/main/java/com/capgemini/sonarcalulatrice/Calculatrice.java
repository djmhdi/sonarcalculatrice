package com.capgemini.sonarcalulatrice;

public class Calculatrice {

	public int addition(int nb1, int nb2) {
		return nb1+nb2;
	}
	
	public int soustraction(int nb1, int nb2) {
		return nb1-nb2;
	}
	
	public int multiplication(int nb1, int nb2) {
		return nb1*nb2;
	}
	
	public int division(int nb1, int nb2) {
		return nb1/nb2;
	}
}
